export default {
    props: {
        img: {
            type: String,
            default: () => ''
        },
        title: {
            type: String,
            default: () => ''
        }
    }
}